import infBut from './assets/information-button 1.png';
import fire from './assets/f.png'
import arrow from './assets/arrow.png'
import './App.css';
import { useEffect, useState } from 'react';
import { format } from "date-fns";


function App() {

  const [bonus, setBonusData] = useState(null);
  const [dateForm, setDateForm] = useState(null)
  const dateFormate = (custDate) => {
    const date = new Date(custDate)
    const formattedDate = format(date, "dd.MM")
    return formattedDate
  }

  const loadData = async (token) => {

    const bonusDataUrl = `http://84.201.188.117:5003/api/v3/ibonus/generalinfo/${token}`;

    const bonusDataResponse = await fetch(bonusDataUrl, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'AccessKey': '891cf53c-01fc-4d74-a14c-592668b7a03c',
      },
    });

    const bonusData = await bonusDataResponse.json();
    setBonusData(bonusData);
    const formatedDate = dateFormate(bonusData.data.dateBurning)
    setDateForm(formatedDate)
    console.log(bonusData)
  }

  const getAccessToken = async () => {
    try {
      const accessKey = '891cf53c-01fc-4d74-a14c-592668b7a03c';
      const accessTokenUrl = 'http://84.201.188.117:5021/api/v3/clients/accesstoken';

      const response = await fetch(accessTokenUrl, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'AccessKey': accessKey,
        },
        body: JSON.stringify({
          idClient: "2c44d8c2-c89a-472e-aab3-9a8a29142315",
          accessToken: "891cf53c-01fc-4d74-a14c-592668b7a03c",
          paramName: "device",
          paramValue: "7db72635-fd0a-46b9-813b-1627e3aa02ea",
          latitude: 0,
          longitude: 0,
          sourceQuery: 0
        }),
      });

      const data = await response.json();
      const str = JSON.stringify(data)
      console.log(str + 'ds')

      loadData(data.accessToken)

    } catch (error) {
      console.error('Fetch Error:', error);
    }
  };


  useEffect(() => {

    getAccessToken();
  }, []);


  return (
    <div className="App">
      <header>
        <h2>ЛОГОТИП</h2>
        <img src={infBut} alt='info' className='info-but'></img>
      </header>
      {bonus ? (
        <article className='bonus-container'>
  
        <div className='text-cont'>
          <h3>{JSON.stringify(bonus.data.currentQuantity)} бонусов</h3>
          <div className='par-cont'>
            <p>{dateForm} сгорит</p>
            <img className='fire-img' src={fire} alt='fire' ></img>
            <p>{JSON.stringify(bonus.data.forBurningQuantity)} бонусов</p>
          </div>
        </div>
        <img src={arrow} alt='arrow' className='arrow-img'></img>
      </article>
      ) : (
      <article className='error'>
        Network Error
      </article>
      )}
      
      <div className='red-back'>
      </div>
    </div>
  );
}

export default App;
